require('app-module-path').addPath(__dirname);
var express = require('express');
var mongoose = require('mongoose');
var events = require('events');
var eventEmitter = new events.EventEmitter();

var startApp = function () {
    var app = express();

    require('bootstrap/express')(app);
    require('controllers/index')(app);
    app.listen(app.get('port'), function () {
        console.log("Server listening on port %d in %s mode", app.get('port'), app.settings.env);
    });
};

require('bootstrap/db')(eventEmitter);
eventEmitter.once('db-connection-established', startApp);