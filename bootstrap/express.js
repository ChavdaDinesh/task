require('dotenv').config()
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cors = require('cors');

module.exports = function (app, config) {

    app.use(bodyParser.json({limit: '5mb'}));

    app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

    app.use(cookieParser());

    app.use(methodOverride());

    app.use(cors());

    app.set('port', process.env.PORT || 3000);
};