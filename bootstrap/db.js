require('dotenv').config()
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;

function loadModels() {
    require('models/user');
    require('models/auth');
    require('models/order');
}

module.exports = function(eventEmitter) {
    loadModels();

    function connect() {
        let options = {
            "useNewUrlParser": true,
            "auto_reconnect": true,
            "socketOptions": {
                "keepAlive": 120
            }
        };
        mongoose.connect(process.env.mongodbUri, options);
    }

    connect();

    mongoose.connection.on('disconnected', connect);

    mongoose.connection.on('error', console.error.bind(console, 'connection error:', process.env.mongodbUri));

    mongoose.connection.once('open', function dbConnectionOpenCallback() {
        console.log('db connected-->', process.env.mongodbUri);
        eventEmitter.emit('db-connection-established');
    });

    return mongoose;
};