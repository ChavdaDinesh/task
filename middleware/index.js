const mongoose = require('mongoose');
const TokenModel = mongoose.model('token');
module.exports = {
    middlewareAuth
};
async function middlewareAuth(req, res, next) {
    try {
        let token = req.headers['authorization'];
        if (!token) {
            res.status(401);
            res.send({
                success: false,
                message: 'Please pass auth Token'
            });
        }
        if (token) {
        	let split = token.split("Bearer ");
            let tokenExist = await TokenModel.findOne({
                token: split[1] || ""
            }).exec();
            if (!tokenExist) {
                res.status(401);
                res.send({
                    success: false,
                    message: 'invalid Token'
                });
            } else{
                req.userId = tokenExist.userId;
            	next();
            }
        }
    } catch (error) {
        console.log(error);
    }
}