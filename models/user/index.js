const mongoose = require('mongoose');
var Schema = mongoose.Schema;

let schemaFeilds = require('./schema');
var userSchema = new Schema(schemaFeilds);

userSchema.statics = {
    createUser
};
let UserModel  = mongoose.model('user', userSchema);
module.exports = UserModel;


async function createUser(newUserData){
	let user = new UserModel(newUserData);
	return await user.save();
}



