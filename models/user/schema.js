let fields = {
    phoneNumber: {
        type: String,
        require: true,
        unique: true,
        validate: phoneNumberValidation,
    },
    address:{},
    name: {
        type: String
    },
    email: {
        type: String,
        validate: ValidateEmail,
    },
    created: {
        type: Date,
        default: Date.now()
    },
    isActive:{
        type: Boolean,
        default: true
    },
    updated: {
        type: Date
    }
};


module.exports = fields;

function phoneNumberValidation(inputtxt) {
    if (!inputtxt) {
        return false;
    }
    var phoneNo = /^\d{10}|\d{13}$/;
    if (inputtxt.match(phoneNo)) {
        return true;
    } else {
        return false;
    }
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return true;
    }
    return false;
}