require('dotenv').config()
const mongoose = require('mongoose');
const crypto = require('crypto');
var Schema = mongoose.Schema;
let fields = {
    userId: {
        type: Schema.Types.ObjectId,
        Ref:'user',
    },
    token: {
        type: String,
        require: true
    },
    created: {
        type: Date,
        expires: process.env.tokenExpTime || 300, // TTL for expire token
        default: Date.now() 
    }
};

var AuthSchema = new Schema(fields);

AuthSchema.statics = {
    createToken
};
let AuthModel = mongoose.model('token', AuthSchema);
module.exports = AuthModel;


async function createToken(userId) {
    let tokenValue = crypto.randomBytes(32).toString('hex');
    let authToken = new AuthModel({
        token: tokenValue,
        userId
    });
    return await authToken.save();
}