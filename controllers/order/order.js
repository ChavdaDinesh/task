const mongoose = require('mongoose');
const orderModel = mongoose.model('order');
const fs = require('fs')

module.exports = {
    addOrder,
    getOrdersList
};

async function addOrder(req) {
    let orderDetails = req.body;
    orderDetails.userId = req.userId;
    return await orderModel.create(orderDetails);
}
async function getOrdersList(userId) {
    let orderList = await orderModel.find({userId}).exec();
    return orderList;
}