var express = require('express');
var router = express.Router();
let orderController = require('./order');

router.post('/add', async function(req, res){
	try{
		let data = await orderController.addOrder(req);
		res.json({
            success: true,
            data
         });
	} catch (error){
		console.log(error);
		res.status(400);
		res.json({
            success: false,
            message: error.message
        });
	}
});
router.get('/getall', async function(req, res){
	try{
		let data = await orderController.getOrdersList(req.userId);
		res.json({
            success: true,
            dataSize: data.length || 0,
            data
         });
	} catch (error){
		console.log(error);
		res.status(400);
		res.json({
            success: false,
            message: error.message
        });
	}
});

module.exports = router;