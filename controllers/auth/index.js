var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const TokenModel = mongoose.model('token');
const UserModel = mongoose.model('user');

router.post('/', async function(req, res){
	try {
		let {phoneNumber} = req.body;
		if(!phoneNumber){
			throw new Error("enter phoneNumber");
		}
		let isExist = await UserModel.findOne({phoneNumber}).exec();
		if(!isExist){
			throw new Error("enter registered phone number");
		}
		await TokenModel.remove({phoneNumber}).exec();
		let data = await TokenModel.createToken(isExist.id);
			res.json({
            success: true,
            data: `Bearer ${data.token}`
         });
	} catch (error){
		console.log(error);
		res.status(400);
		res.json({
            success: false,
            message: error.message
        });
	}
});


module.exports = router;