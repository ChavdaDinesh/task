var express = require('express');
var router = express.Router();
let userController = require('./user');
router.post('/add', async function(req, res){
	try{
		let data = await userController.addUser(req);
		res.json({
            success: true,
            data
         });
	} catch (error){
		console.log(error);
		res.status(400);
		res.json({
            success: false,
            message: error.message
        });
	}
});
router.get('/getall', async function(req, res){
	try{
		let data = await userController.getUserList();
		res.json({
            success: true,
            dataSize: data.length || 0,
            data
         });
	} catch (error){
		console.log(error);
		res.status(400);
		res.json({
            success: false,
            message: error.message
        });
	}
});

module.exports = router;