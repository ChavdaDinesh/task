const mongoose = require('mongoose');
const UserModel = mongoose.model('user');
const fs = require('fs')

module.exports = {
    addUser,
    getUserList
};

async function addUser(req) {
    let userDetails = req.body;
    if (!userDetails.phoneNumber) {
        throw new Error("phoneNumber require");
    }
    if (!userDetails.email) {
        throw new Error("email address require");
    }
    if (!userDetails.isActive) {
        userDetails.isActive = false;
    }
    let useExist = await UserModel.findOne({
        phoneNumber: userDetails.phoneNumber
    }).exec();
    if (useExist) {
        throw new Error("User already registered with this phoneNumber");
    }
    return await UserModel.createUser(userDetails);
}
async function getUserList() {
    let userList = await UserModel.find({}).exec();
    return userList;
}