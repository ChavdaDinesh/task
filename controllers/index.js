let userRouter = require('./user/index')
let authRouter = require('./auth/index');
let orderRouter = require('./order/index');
let auth = require('middleware');
module.exports = function mainController(app) {
    app.use('/users', userRouter);
    app.use('/generateToken', authRouter);
    app.use('/order', auth.middlewareAuth, orderRouter);
    app.use(function postHandler(req, res, next) {
        if (req.route && req.route.path) {
            res.json({
                success: true,
                resultSize: res.resultSize,
                data: res.data
            });
            res.end();
            return;
        } else {
            next();
        }
    });
    app.use(function notFoundHandler(req, res, next) {
        if (!req.route || !req.route.path) {
            console.log(req.params);
            res.status(404);
            console.log('%s %d %s', req.method, res.statusCode, req.url);
            return res.json({
                error: 'Not found'
            });
        }
    });
};